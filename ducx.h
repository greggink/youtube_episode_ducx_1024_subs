#ifndef DUCX_H
#define DUCX_H

//suppress some compiler warnings
#pragma GCC diagnostic ignored "-Wunused-result"

#include <stdio.h>
#include "technical.h"
#ifdef __linux
#include "tc.h"
#endif
#include "logo.h"

#define DUC_MAX_STRING_SIZE 4095
#define DUC_MAX_STRING_SIZE_INC 4096
#define DUC_MAX_NUMBER_VARS 127

//10 MB stack size
#define DUC_STACK_SIZE 10485760

//TODO(GI):
//allow for arbitrary long programs
//with many more loc than 1024
#define DUC_LOC_IN_BLOCK 1024

//types of tokens, used during parsing
enum tok_type{DUC_TOK_ERROR, DUC_TOK_FUNC_NAME, DUC_TOK_DATATYPE, DUC_TOK_VAR_NAME, DUC_TOK_STRING, DUC_TOK_NUMBER, DUC_TOK_RETURN, DUC_TOK_ELSE, DUC_TOK_OTHER_KEYWORD, DUC_TOK_EQUAL, DUC_TOK_MATH_OP, DUC_TOK_REL_OP, DUC_TOK_OPENING_BLOCK, DUC_TOK_CLOSING_BLOCK, DUC_TOK_DELIMETER, DUC_TOK_COMMA, DUC_TOK_OPEN_BRACKET, DUC_TOK_CLOSED_BRACKET, DUC_TOK_LINE_COMMENT, DUC_TOK_RENEGED, DUC_TOK_EOF};

//different types of operations
//naming conventions
//some ops end in _VV _VC _CV
//variable with variable (_VV)
//variable with constant (_VC)
//constant with variable (_CV)
//L refers to the return value of another line of code (LOC)
//etc
//All operations potentially part of an expression are found
//between DUC_OP_BEGIN_OP and DUC_OP_END_OP
//this is so I can write code
// if( l->type > DUC_OP_BEGIN_OP && l->type < DUC_OP_END_OP ){
//		...
//}
//
enum op_type{DUC_OP_NOP, DUC_OP_CREATE_VAR_L, DUC_OP_CREATE_VAR_V, DUC_OP_CREATE_VAR_C, DUC_OP_INT_ASSIGN_L, DUC_OP_INT_ASSIGN_V, DUC_OP_INT_ASSIGN_C, DUC_OP_LINLOC, DUC_OP_FUNC_0, DUC_OP_FUNC_3, DUC_OP_FUNC_11, DUC_OP_FUNC_BODY, DUC_OP_RETURN, DUC_OP_WHILE, DUC_OP_IF, DUC_OP_IF_ELSE, DUC_OP_JUMP, DUC_OP_BEGIN_OP, DUC_OP_FUNC_CALL_3, DUC_OP_INT_ADD_LL, DUC_OP_INT_ADD_LV, DUC_OP_INT_ADD_LC, DUC_OP_INT_ADD_VL, DUC_OP_INT_ADD_VV, DUC_OP_INT_ADD_VC, DUC_OP_INT_ADD_CL, DUC_OP_INT_ADD_CV, DUC_OP_INT_ADD_CC, DUC_OP_INT_SUB_LL, DUC_OP_INT_SUB_LV, DUC_OP_INT_SUB_LC, DUC_OP_INT_SUB_VL, DUC_OP_INT_SUB_VV, DUC_OP_INT_SUB_VC, DUC_OP_INT_SUB_CL, DUC_OP_INT_SUB_CV, DUC_OP_INT_SUB_CC, DUC_OP_INT_MUL_LL, DUC_OP_INT_MUL_LV, DUC_OP_INT_MUL_LC, DUC_OP_INT_MUL_VL, DUC_OP_INT_MUL_VV, DUC_OP_INT_MUL_VC, DUC_OP_INT_MUL_CL, DUC_OP_INT_MUL_CV, DUC_OP_INT_MUL_CC, DUC_OP_INT_DIV_LL, DUC_OP_INT_DIV_LV, DUC_OP_INT_DIV_LC, DUC_OP_INT_DIV_VL, DUC_OP_INT_DIV_VV, DUC_OP_INT_DIV_VC, DUC_OP_INT_DIV_CL, DUC_OP_INT_DIV_CV, DUC_OP_INT_DIV_CC, DUC_OP_INT_MOD_LL, DUC_OP_INT_MOD_LV, DUC_OP_INT_MOD_LC, DUC_OP_INT_MOD_VL, DUC_OP_INT_MOD_VV, DUC_OP_INT_MOD_VC, DUC_OP_INT_MOD_CL, DUC_OP_INT_MOD_CV, DUC_OP_INT_MOD_CC, DUC_OP_REL_EQ_LL, DUC_OP_REL_EQ_LV, DUC_OP_REL_EQ_LC, DUC_OP_REL_EQ_VL, DUC_OP_REL_EQ_VV, DUC_OP_REL_EQ_VC, DUC_OP_REL_EQ_CL, DUC_OP_REL_EQ_CV, DUC_OP_REL_EQ_CC, DUC_OP_END_OP};
enum var_type{DUC_VAR_VOID, DUC_VAR_INT};

#define DUC_FLAGS_RETURN_VAR 1
#define DUC_FLAGS_RETURN_LOC 2
#define DUC_FLAGS_FIRST_LOC 1
#define DUC_FLAGS_SECOND_LOC 2
#define DUC_FLAGS_THIRD_LOC 4

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Documentation duCx bytecode
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
loc: line of code; bytecode is nothing but a list of loc; see struct below
v->seq: sequence in which variables will appear on stack

op_types and how they are encoded in loc:
*****************************************
0: DUC_OP_NOP
	nop: not an operation
	does nothing but does it well

1: DUC_OP_CREATE_VAR_L:
	create variable and initialize with return value of another loc
	l->operand1 = v->seq;
	l->operand2 = (loc *) previous_loc;

2: DUC_OP_CREATE_VAR_V:
	l->operand1 = v->seq;
	l->operand2 = v->seq of variable to be assigned????

3: DUC_OP_CREATE_VAR_C:
	l->operand1 = v->seq;
	l->operand2 = constant value to be assigned

7: DUC_OP_LINLOC:
	linloc: load into next loc

8: DUC_OP_FUNC_CALL_3:
	l->operator.jump_to + first_loc = place to jump to

13: DUC_OP_RETURN:
	l->operator.jump_to + first_loc = place to jump to

*/

typedef struct _ducx ducx;
typedef struct _loc loc;
typedef struct _loc_long loc_long;
typedef struct _token token;
typedef struct _var var;
typedef struct _term term;
typedef struct _func func;

struct _func{

	loc *l;
	int num_params;
	char *name;
	int name_len;

};

struct _term{

	var *v[4];
	char op;	//operation
	loc *ret;
	union {
		int64_t i64;
		int32_t i;
		float f;
		double d;
	} value[4];
	func *f;
	loc *operands[3];

};

struct _var{

	union {
		int64_t i64;
		int i;
		float f;
		char *str;
		loc *l;
	} value;
	int type;
	int len;
	int seq;	//number in sequence on stack for function scope

};

union {

	var *v;
	struct{
		int32_t h;	//high bytes
		int32_t l;	//low bytes
	};

} operand1;

//loc: line of code
//size: 64 bytes
struct _loc{

	uint32_t flags;
	uint32_t type;
	int32_t line_nr;
	union{
		int op_func;
		int jump_to;
	}operator;

	union {
		var *v;
		loc *l;
		int64_t i64;
	} operand1;

	union {
		var *v;
		loc *l;
		int64_t i64;
	} operand2;

	union {
		var *v;
		loc *l;
		int64_t i64;
	} operand3;

//	operand operand1;
//	operand *operand2;
//	operand *operand3;
	var ret;	//return value of the loc

};

struct _loc_long{

	uint32_t flags;
	uint32_t type;
	uint64_t operator;
	var *operand[11];
	var ret;	//return value of the loc

};

struct _token{

	int type;
	char *start;
	int len;
	int empty_func;
	int line_nr;

	char *start_next;	//start of next token
	char *start_previous;	//for reneging tokens

};

struct _ducx{

	//used to keep track of parsing to bytecode
	int ii;	//instruction index

	//bytecode
	loc block[DUC_LOC_IN_BLOCK];

	int line_nr;
	char *buffer;

	//keep track of nested control structures
	//control structure pointers: while, if
	//also for functions
	loc *csp[127];
	int csp_counter;
	loc *conditions[127];

	//list of variables
	var v_list[DUC_MAX_NUMBER_VARS];
	char v_list_names[DUC_MAX_NUMBER_VARS][32];
	int v_list_names_length[DUC_MAX_NUMBER_VARS];
	int num_vars;

	unsigned char stack[DUC_STACK_SIZE];

	func functions[1000];
	int last_function;

	//for parsing
	int in_function;

};

//////////////////////////////////
// prototypes of DuCx

//logo and info printing functions
void duc_print_logo_row(uint32_t *pixels, int cols, int row);
void duc_print_info();

//parsing functions
char* duc_skip_shebang(char *buffer);
void duc_parse_code(char *buffer, ducx *d);
loc* duc_parse_get_next_loc(ducx *d);
loc* duc_parse_peek_next_loc(ducx *d);
token* duc_parse_get_next_token(ducx *d, token *tok);
#define duc_get_next_char(D, B) {if(*B == '\n'){	D->line_nr += 1;} B += 1;}
token* duc_parse_renege_token(token *tok);
void duc_parse_function(ducx *d, token *tok);
void duc_parse_return(ducx *d, token *tok);
void duc_parse_prototype(ducx *d, token *tok);
void duc_parse_var_clear_list(ducx *d);
var* duc_parse_var_add_list(ducx *d, token *tok);
var* duc_parse_get_var(ducx *d, token *tok);
loc* duc_parse_custom_function(ducx *d, token *tok, int level);
func* duc_parse_find_custom_function(ducx *d, token *tok);
void duc_parse_create_variable(ducx *d, token *tok);
void duc_parse_statement(ducx *d, token *tok);
loc* duc_parse_expression(ducx *d, token *tok, int level, int restrict_to_one);
void duc_parse_process_parameters(ducx *d, int num_params, loc **parameters, uint64_t *final_operands);
void duc_parse_while(ducx *d, token *tok);
void duc_parse_if(ducx *d, token *tok);
void duc_parse_else(ducx *d, token *tok, loc *last_if);
void duc_parse_print_string(ducx *d, token *tok);
void duc_parse_print_int(ducx *d, token *tok);
void duc_parse_clear_screen(ducx *d, token *tok);
void duc_parse_exit(ducx *d, token *tok);
void duc_parse_print_error(ducx *d, token *tok);

//bytecode functions
void duc_run_bytecode(ducx *d);
void duc_exec_print_string(loc *l, var *ret);
void duc_exec_print_int(loc *l, var *ret);
void duc_exec_clear_screen(var *ret);
void duc_exec_exit(loc *l, var *ret);
void duc_exec_print_error(loc *l, var *ret);

char* duc_string_unescape_double_quotes_and_others(char *str);
int duc_check_datatype(token *tok);
int duc_check_keyword(token *tok);

void* built_in_functions[99] = { 0, &duc_exec_clear_screen, &duc_exec_print_string, &duc_exec_print_int, &duc_exec_print_error, &duc_exec_exit};
/////////////////////////////////

#endif