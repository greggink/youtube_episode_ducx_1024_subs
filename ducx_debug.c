#ifndef DUCX_DEBUG_C
#define DUCX_DEBUG_C

#include "ducx.h"

/*NOTE(GI):
	code in this file was written a little at a time as I felt the need for it
	This code has not been as scrutinized as the main.c
	May not work anymore
*/

void duc_debug_display_stack(ducx *d, int64_t *st, int64_t *bp, int64_t *vars);
void duc_debug_display_token(token *tok);
void duc_debug_print_block(ducx *d, loc *l, uint32_t number);
void duc_debug_display_terms(term *t);

void duc_debug_display_stack(ducx *d, int64_t *st, int64_t *bp, int64_t *vars){

	uint64_t *tmp = (uint64_t *) d->stack;

	printf("%sstack display:%s\n", TEC_PRINT_GREEN, TEC_PRINT_NORMAL);
	printf("***************\n");
	printf("d->stack is %lx\n", (uint64_t) d->stack);
	printf("st is %lx (%d)\n", (uint64_t) st, (uint32_t) (st - (int64_t *) d->stack) );
	printf("bp is %lx and *bp is %lx\n", (uint64_t) bp, (uint64_t) *bp);
	printf("vars is %lx\n", (uint64_t) vars);

	while(tmp < (uint64_t *) st){
		printf("%lx: %lx (%ld)\n", (uint64_t) tmp, *tmp, *tmp);
		tmp += 1;
	}

	printf("%s***************%s\n", TEC_PRINT_GREEN, TEC_PRINT_NORMAL);

}//duc_debug_display_stack*/

void duc_debug_display_token(token *tok){

	if(!tok)
		return;

	char *tmp = tok->start;
	int len = tok->len;

	printf("Token:%s\n", TEC_PRINT_YELLOW);
	switch(tok->type){
	case DUC_TOK_ERROR:
		printf("DUC_TOK_ERROR");
		break;
	case DUC_TOK_FUNC_NAME:
		printf("DUC_TOK_FUNC_NAME");
		break;
	case DUC_TOK_DATATYPE:
		printf("DUC_TOK_DATATYPE");
		break;
	case DUC_TOK_VAR_NAME:
		printf("DUC_TOK_VAR_NAME");
		break;
	case DUC_TOK_STRING:
		printf("DUC_TOK_STRING");
		break;
	case DUC_TOK_NUMBER:
		printf("DUC_TOK_NUMBER");
		break;
	case DUC_TOK_RETURN:
		printf("DUC_TOK_RETURN");
		break;
	case DUC_TOK_ELSE:
		printf("DUC_TOK_ELSE");
		break;
	case DUC_TOK_OTHER_KEYWORD:
		printf("DUC_TOK_OTHER_KEYWORD");
		break;
	case DUC_TOK_EQUAL:
		printf("DUC_TOK_EQUAL");
		break;
	case DUC_TOK_MATH_OP:
		printf("DUC_TOK_MATH_OP");
		break;
	case DUC_TOK_OPENING_BLOCK:
		printf("DUC_TOK_OPENING_BLOCK");
		break;
	case DUC_TOK_CLOSING_BLOCK:
		printf("DUC_TOK_CLOSING_BLOCK");
		break;
	case DUC_TOK_DELIMETER:
		printf("DUC_TOK_DELIMETER");
		break;
	case DUC_TOK_COMMA:
		printf("DUC_TOK_COMMA");
		break;
	case DUC_TOK_OPEN_BRACKET:
		printf("DUC_TOK_OPEN_BRACKET");
		break;
	case DUC_TOK_CLOSED_BRACKET:
		printf("DUC_TOK_CLOSED_BRACKET");
		break;
	case DUC_TOK_LINE_COMMENT:
		printf("DUC_TOK_LINE_COMMENT");
		break;
	case DUC_TOK_EOF:
		printf("DUC_TOK_EOF");
		break;
	}

	printf("\n%stext is *", TEC_PRINT_NORMAL);
	while(len){
		putchar(*tmp);
		len -= 1;
		tmp += 1;
	}
	printf("*\n");

}//duc_debug_display_token*/

void duc_debug_print_block(ducx *d, loc *l, uint32_t number){

	if(!l){
		l = &(d->block[0]);
	}

	int i = 0;
	loc *first = &(d->block[0]);
	int tmp = 0;
	if(!number){
		number = -1;
	}

	while(l->type && i < number){
		printf("loc:\n");
		printf("%s%d%s ", TEC_PRINT_GREEN, i, TEC_PRINT_NORMAL);
		switch(l->type){
		case DUC_OP_NOP:
			printf("no operation\n");
			break;
		case DUC_OP_INT_ASSIGN_V:
			printf("assign variable to variable\n");
			break;
		case DUC_OP_INT_ASSIGN_C:
			printf("constant assignment\n");
			break;
		case DUC_OP_LINLOC:
			printf("linloc\n");
			if(l->operand1.i64){
				printf("\tvar 1 (%d)\n", (int) l->operand1.i64);
			}
			if(l->operand2.i64){
				printf("\tvar 2 (%d)\n", (int) l->operand2.i64);
			}
			if(l->operand3.i64){
				printf("\tvar 3 (%d)\n", (int) l->operand3.i64);
			}
			break;
		case DUC_OP_FUNC_0:
			printf("function 0 args\n");
			break;
		case DUC_OP_FUNC_3:
			printf("function up to 3 args\n");
			break;
		case DUC_OP_FUNC_11:
			printf("function 11 args\n");
			break;
		case DUC_OP_FUNC_CALL_3:
			tmp = l->operator.jump_to;
			printf("function call (3)%s%d%s\n", TEC_PRINT_GREEN, tmp, TEC_PRINT_NORMAL);
			break;
		case DUC_OP_CREATE_VAR_L:
			printf("create variable, assign loc return value\n");
			break;
		case DUC_OP_CREATE_VAR_V:
			printf("create variable, assign variable value\n");
			break;
		case DUC_OP_CREATE_VAR_C:
			printf("create variable, assign constant\n");
			break;
		case DUC_OP_FUNC_BODY:
			printf("function body start\n");
			break;
		case DUC_OP_JUMP:
			tmp = l->operator.jump_to;
			printf("jumping to... %s%d%s\n", TEC_PRINT_GREEN, tmp, TEC_PRINT_NORMAL);
			break;
		case DUC_OP_WHILE:
			printf("while loop\n");
			break;
		case DUC_OP_IF:
			printf("if\n");
			break;
		case DUC_OP_INT_ADD_VC:
		case DUC_OP_INT_ADD_CV:
		case DUC_OP_INT_ADD_VV:
			printf("integer addition\n");
			break;
		case DUC_OP_INT_ADD_CC:
			printf("integer addition, constant with constant\n");
			break;
		case DUC_OP_INT_ADD_CL:
			printf("integer addition, constant with loc\n");
			break;
		case DUC_OP_INT_SUB_VV:
		case DUC_OP_INT_SUB_VC:
		case DUC_OP_INT_SUB_CV:
		case DUC_OP_INT_SUB_CC:
			printf("integer subtraction\n");
			break;
		case DUC_OP_INT_MUL_VC:
		case DUC_OP_INT_MUL_CV:
		case DUC_OP_INT_MUL_VV:
		case DUC_OP_INT_MUL_CC:
			printf("integer multiplication\n");
			break;
		case DUC_OP_INT_DIV_VV:
		case DUC_OP_INT_DIV_VC:
		case DUC_OP_INT_DIV_CV:
		case DUC_OP_INT_DIV_CC:
			printf("integer division\n");
			break;
		case DUC_OP_RETURN:
			printf("returning from function starting at ");
			if(l->operator.jump_to){
				tmp = l->operator.jump_to;
				printf("%s%d%s\n", TEC_PRINT_GREEN, tmp, TEC_PRINT_NORMAL);
			}else{
				printf("no return value\n");
			}
			break;
		default:
			printf("l->type is %d\n", l->type);
			break;
		}

		printf("\n***************\n");
		l += 1;
		i += 1;
	}

}//duc_debug_print_block*/

void duc_debug_display_terms(term *t){

	int i = 0;

	while(t[i].op){
		i += 1;
	}
	printf("%d term(s) detected\n", i);

	i = 0;
	while(t[i].op){
		if(t[i].op == ' '){
			printf("*processed*\n");
		}else{
			printf("operation here %c\n", t[i].op);
		}
		if(!t[i].v[0]){
			printf("int is %d\n", t[i].value[0].i);
		}
		i += 1;
	}

}//duc_debug_display_terms*/

#endif