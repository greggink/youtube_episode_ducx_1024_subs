#ifndef DUCX_ERROR
#define DUCX_ERROR

#include "technical.h"

/*duc_error:
	prints an error string to STDERR

	supports %d, %c and %s, just like in printf
	that's all it supports right now
	so, no %g or other formats like %2d

	TODO(GI):
	1. expands with more formatting options

*/
#define duc_user_error(...) d_error(1, 0, 0, __VA_ARGS__);
#define duc_error(...) d_error(0, __FILE__, __LINE__, __VA_ARGS__);

void d_error(int user, char *file, int line, char *error_msg, ...){

	va_list vl;

	char *tmp = NULL;
	int len = 0;
	char percent[2] = "%";
	char *string = error_msg;

#ifdef __linux

	int arr_size = 8;
	int i = 1; //not zero, because prefix

	////prefix
	char prefix[100] = "\x1B[1;31mduCx error:\x1B[0m";
	if(!user && file){
		tec_string_cat(prefix, " (\x1B[1;33m", 100);
		tec_string_cat(prefix, file, 100);
		if(line){
			tec_string_cat(prefix, ":", 100);
			len = tec_string_length(prefix);
			tmp = prefix + len;
			tec_string_from_int(line, tmp, 100 - len);
		}
		tec_string_cat(prefix, "\x1B[0m): ", 100);
	}else{
		tec_string_cat(prefix, "\x1B[0m ", 100);
	}

	char str_null[24] = "\x1B[0;36m<NULL>\x1B[0m";
	struct iovec iov[arr_size];	//size is multiple of cache-line on 64bit
	iov[0].iov_base = prefix;
	iov[0].iov_len = tec_string_length(prefix);
	char buf_integer[arr_size][64];

	if(!string || !*string){
		write(2, str_null, 17 );
		write(2, "\n", 1);
		return;
	}

#endif

#ifdef __WIN32

	DWORD bytes_written = 0;

	char fd_prefix[100] = "tec_error (";
	tec_string_cat(fd_prefix, file, 100);
	tec_string_cat(fd_prefix, ":", 100);
	int fd_len = tec_string_length(fd_prefix);
	tmp = fd_prefix + fd_len;
	tec_string_from_int(line, tmp, 100 - fd_len);
	tec_string_cat(fd_prefix, "): ", 100);
	char fd_str_null[7] = "<NULL>";

	HANDLE h_out = GetStdHandle(STD_ERROR_HANDLE);	
	WriteConsole(h_out, fd_prefix, tec_string_length(fd_prefix), &bytes_written, 0);

	if(!string || !*string){
		WriteConsole(h_out, fd_str_null, 6, &bytes_written, 0);
		WriteConsole(h_out, "\n", 1, &bytes_written, 0);
		return;
	}

#endif

	char fd_buf_integer[32];
	va_start(vl, error_msg);
	char *str = NULL;
	int32_t integer32 = 0;
	char c;
	char *fd_str = NULL;
	int err = 0;

	while(*string){
		if(*string == '%'){
			string += 1;
////////////////////////////////////////////////////////
// The switch for Linux
////////////////////////////////////////////////////////
#ifdef __linux
			switch(*string){
			case 's':
				string += 1;
				str = va_arg(vl, char *);

				if(!str || !*str){
					str = str_null;
				}else{
					iov[i].iov_base = TEC_PRINT_YELLOW_DARK;
					iov[i].iov_len = tec_string_length(TEC_PRINT_YELLOW_DARK);
					i += 1;
					iov[i].iov_base = str;
					iov[i].iov_len = tec_string_length(str);
					i += 1;
					str = TEC_PRINT_NORMAL;
				}
				len = tec_string_length(str);
				break;
			case 'd':
				string += 1;
				integer32 = va_arg(vl, int32_t);
				tec_string_from_int(integer32, fd_buf_integer, 32);
				if(integer32 < 0){
					tec_string_copy(buf_integer[i], "\x1B[38;2;255;164;0m", 64);
				}else{
					tec_string_copy(buf_integer[i], "\x1B[38;2;255;200;100m", 64);
				}
				tec_string_cat(buf_integer[i], fd_buf_integer, 64);
				tec_string_cat(buf_integer[i], "\x1B[m", 64);
				str = buf_integer[i];
				len = tec_string_length(str);
				break;
			case 'c':
				string += 1;
				//va_arg won't accept 'char', so we use int32_t
				c = (char) va_arg(vl, int32_t);
				tec_string_copy(buf_integer[i], "\x1B[38;2;255;164;0m", 64);
				buf_integer[i][17] = c;
				buf_integer[i][18] = 0;
				tec_string_cat(buf_integer[i], "\x1B[m", 64);
				str = buf_integer[i];
				len = 21;
				break;
			case '%':
				string += 1;
				str = percent;
				len = 1;
				break;
			default:
				//TODO(GI):
				//handle this case
				break;
			}
#endif

////////////////////////////////////////////////////////
// The switch for Windows
////////////////////////////////////////////////////////
#ifdef __WIN32

			switch(*string){
			case 's':
				string += 1;
				str = va_arg(vl, char *);
				fd_str = str;

				if(!str || !*str){
					fd_str = fd_str_null;
				}

				fd_len = tec_string_length(fd_str);
				break;
			case 'd':
				string += 1;
				integer32 = va_arg(vl, int32_t);
				tec_string_from_int(integer32, fd_buf_integer, 32);
				fd_str = fd_buf_integer;
				fd_len = tec_string_length(fd_str);
				break;
			case 'c':
				string += 1;
				//va_arg won't accept 'char', so we use int32_t
				c = (char) va_arg(vl, int32_t);
				fd_buf_integer[0] = c;
				fd_buf_integer[1] = 0;
				fd_str = fd_buf_integer;
				fd_len = 1;
				break;
			case '%':
				string += 1;
				fd_str = percent;
				fd_len = 1;
				break;
			default:
				//TODO(GI):
				//handle this case
				break;
			}
#endif

		}else{

			str = string;
			len = 0;

			while(*string && *string != '%'){
				string += 1;
				len += 1;
			}
#ifdef __WIN32
			fd_str = str;
			fd_len = len;
#endif
		}

#ifdef __WIN32
		WriteConsole(h_out, fd_str, fd_len, &bytes_written, 0);
#endif

#ifdef __linux
		iov[i].iov_base = str;
		iov[i].iov_len = len;
		i += 1;
		if(i == arr_size){
			err = writev(2, iov, i);
			if(err == -1){
				return;
			}
			i = 0;
		}
#endif

	}

#ifdef __linux

	err = writev(2, iov, i);
	if(err == -1)
		return;
	write(2, "\n", 1);

#endif

#ifdef __WIN32
	WriteConsole(h_out, "\n", 1, &bytes_written, 0);
#endif

	va_end(vl);

}//d_error*/

#endif