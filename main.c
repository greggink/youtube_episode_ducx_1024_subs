#include "ducx.h"
#include "ducx_debug.c"
#include "ducx_error.c"

int main(int argc, char **argv){

	if(argc < 2){
		duc_user_error("please provide a valid duCx sourcefile\n");
		return 1;
	}

	//looking for flags on command line
	int i = 1;
	int j = 0;
	int just_list = 0;
	while(i < argc && argv[i][0] == '-'){
		j = 0;
		while(argv[i][j]){
			switch(argv[i][j]){
			case 'v':
				duc_print_info();
				return 0;
				break;
			case 'l':
				just_list = 1;
				break;
			}
			j += 1;
		}

		i += 1;
	}

	if( i == argc ){
		duc_user_error("provide a ducx source file\n");
		return 1;
	}

	char *file = argv[i];
	if(!tec_file_exists(file) ){
		duc_user_error("file %s does not appear to exists or cannot be accessed\n", file);
		return 1;
	}

	char *tmp = 0;
	int len = tec_string_length(file);
	tmp = file + len - 4;
	if( !tec_string_equal(tmp, ".duc") ){
		duc_user_error("file %s does not appear to be a duCx's source file\n", file);
		return 1;
	}

	int64_t file_size = tec_file_get_size(file);
	if(!file_size){
		duc_user_error("given file %s is empty\n", file);
		return 1;
	}

	char *buffer = tec_file_get_contents(file);

	if(!buffer){
		duc_user_error("failed to load file %s\n", file);
		return 1;
	}

	//allocate main duCx struct
	ducx *d = (ducx *) malloc(sizeof(ducx));
	memset(d, 0, sizeof(ducx));
	d->buffer = buffer;
	d->line_nr = 1;

	buffer = duc_skip_shebang(buffer);
	if(buffer != d->buffer){
		d->line_nr = 2;
	}
	duc_parse_code(buffer, d);

	if(just_list){
		duc_debug_print_block(d, 0, 0);
		return 0;
	}

	duc_run_bytecode(d);

	return 0;

}//main*/

void duc_print_logo_row(uint32_t *pixels, int cols, int row){
#ifdef __linux
	if(cols < 131){
		printf("\n");
		return;
	}

	row -= 1;
	uint32_t pixel = pixels[0];
	tc_move_cursor(cols - 64, row);

	char color[20];
	int col = 0;
	while(col < 64){
		pixel = pixels[(row*64)+col];
		snprintf(color, 20, "\x1B[48;2;%d;%d;%dm", (pixel&0x00ff0000)>>16, (pixel&0x0000ff00)>>8, (pixel&0x000000ff));
		printf("%s ", color);
		col += 1;
	}

	printf("%s\n\n", TEC_PRINT_NORMAL);
#endif
}//duc_print_logo_row*/

void duc_print_info(){

#ifdef __linux
	tc_clear_screen();
#else
	write(1, "\033[H\033[2J\033[3J", 11);
#endif

	uint32_t *pixels = (uint32_t *) ducx_logo;
	int cols = 0;
	int rows = 0;
#ifdef __linux
	tc_get_cols_rows(&cols, &rows);
#else
	cols = 80;
#endif

	int row = 1;
	int x = 0;
	x = cols - 64;

	row += 1;
#ifdef __linux
	tc_move_cursor(1, row);
#else
	write(1, "\033[H\033[2J\033[3J", 11);
#endif

	printf(" The duCx programming language\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" *****************************\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf("\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" The language which has nothing to do with taking over the world.\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" Version 0.000003\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" Created by Gregg Ink\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf("\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" This language is meant for fun and education.\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" Use in your serious projects at your own risk and peril.\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf("\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" Source code available at https://giblab.com/greggink\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" Discussion of the language on the youtube channel:\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" https://youtube.com/c/gregginkcodes\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf("\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" Twitter: @Gregg_Ink\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	printf(" Reddit: u/gregg_ink\n");
	duc_print_logo_row(pixels, cols, row);

	row += 1;
	if(cols >= 131){
		while(row < 32){
			duc_print_logo_row(pixels, cols, row);
			row += 1;
		}
	}

	printf("\n\n%s", TEC_PRINT_NORMAL);

}//duc_print_info*/

char* duc_skip_shebang(char *buffer){

	if(buffer[0] == '#' && buffer[1] == '!'){
		buffer += 2;
		while(*buffer && *buffer != '\n'){
			buffer += 1;
		}
		if(!*buffer){
			duc_user_error("Did not find new line at end of shebang line\n");
			exit(1);
		}
		buffer += 1;
	}

	return buffer;

}//duc_skip_shebang*/

void duc_parse_code(char *buffer, ducx *d){

	token tokk;
	memset(&tokk, 0, sizeof(token));
	token *tok = &tokk;
	tok->start_next = buffer;
	loc *l = NULL;
	loc *last_if = NULL;
	loc *first_loc = d->block;

	while(tok->type != DUC_TOK_EOF){
		duc_parse_get_next_token(d, tok);
		switch(tok->type){
		case DUC_TOK_FUNC_NAME:
			if(!d->in_function){
				duc_user_error("line %d: function call outside of function", tok->line_nr);
				exit(0);
			}
			duc_parse_function(d, tok);
			break;
		case DUC_TOK_RETURN:
			if(!d->in_function){
				duc_user_error("line %d: keyword return outside of function", tok->line_nr);
				exit(0);
			}
			duc_parse_return(d, tok);
			break;
		case DUC_TOK_ELSE:
			if(!d->in_function){
				duc_user_error("line %d: keyword else outside of function", tok->line_nr);
				exit(0);
			}
			duc_parse_else(d, tok, last_if);
			break;
		case DUC_TOK_OTHER_KEYWORD:
			if(!d->in_function){
				tok->start[tok->len] = 0;
				duc_user_error("line %d: keyword %s outside of function", tok->line_nr, tok->start);
				exit(0);
			}
			duc_parse_function(d, tok);
			break;
		case DUC_TOK_DATATYPE:
			if(d->in_function){
				duc_parse_create_variable(d, tok);
			}else{
				duc_parse_prototype(d, tok);
			}
			break;
		case DUC_TOK_VAR_NAME:
			duc_parse_statement(d, tok);
			break;
		case DUC_TOK_CLOSING_BLOCK:
			l = duc_parse_get_next_loc(d);
			if(!d->csp_counter){
				tok->start[tok->len] = 0;
				duc_user_error("line %d: Incorrect nesting of control structures\n", tok->start);
				exit(0);
			}
			d->csp_counter -= 1;
			loc *l_to = d->csp[d->csp_counter];
			loc *condition = d->conditions[d->csp_counter];
			d->csp[d->csp_counter] = NULL;
			switch(l_to->type){
			case DUC_OP_WHILE:
				l->type = DUC_OP_JUMP;
				l->operator.jump_to = condition - first_loc;
				l_to->operand2.i64 = l + 1 - first_loc;
				last_if = NULL;
				break;
			case DUC_OP_IF:
				l->type = DUC_OP_JUMP;
				l->operator.jump_to = l_to - first_loc;	////!!!!! not condition here
				l_to->operand2.i64 = l + 1 - first_loc;
				last_if = l_to;
				break;
			case DUC_OP_IF_ELSE:
				l->type = DUC_OP_JUMP;
				l->operator.jump_to = l_to - first_loc;	////!!!!! not condition here
				l_to->operand2.i64 = l + 1 - first_loc;
				last_if = NULL;
				break;
			case DUC_OP_FUNC_BODY:
				l->type = DUC_OP_RETURN;
				l->ret.value.i = 1;	//default return value of a function is one
				l->operator.jump_to = l_to - first_loc;
				d->in_function = 0;
				last_if = NULL;
				duc_parse_var_clear_list(d);
				break;
			default:
				tok->start[tok->len] = 0;
				duc_user_error("line %d: strange end of block", tok->start);
				exit(0);
			}
			break;
		case DUC_TOK_EOF:	//intentionally falling through
		case DUC_TOK_LINE_COMMENT:
			//do nothing;
			break;
		default:
			tok->start[tok->len] = 0;
			duc_user_error("line %d: unknown/unexpected token %s\n",tok->line_nr, tok->start);
			exit(0);
			break;
		}
	}

}//duc_parse_code*/

loc* duc_parse_get_next_loc(ducx *d){

	//TODO(GI):
	//allow for arbitrarily long programs
	if(!d->ii){
		d->ii += 1;	//first line reserved for jump to main function
	}
	loc *l = &(d->block[d->ii]);
	d->ii += 1;
	if(d->ii >= DUC_LOC_IN_BLOCK){
		duc_user_error("block full\n");
		exit(1);
	}

	return l;

}//duc_parse_get_next_loc*/

loc* duc_parse_peek_next_loc(ducx *d){

	int ii = d->ii;
	//TODO(GI):
	//allow for arbitrarily long programs
	if(!ii){
		ii += 1;	//first line reserved for jump to main function
	}
	loc *l = &(d->block[ii]);
	ii += 1;
	if(ii >= DUC_LOC_IN_BLOCK){
		duc_user_error("block full\n");
		exit(1);
	}

	return l;

}//duc_parse_peek_next_loc*/

token* duc_parse_get_next_token(ducx *d, token *tok){

	//TODO(GI):
	//should I use a switch to speed up this function???

	char *buffer = tok->start_next;
	tok->start_previous = tok->start;
	tok->empty_func = 0;
	int len = 0;

	while(*buffer && tec_char_is_white_space(*buffer)){
		duc_get_next_char(d, buffer);
	}
	tok->line_nr = d->line_nr;

	if( !(*buffer) ){
		tok->type = DUC_TOK_EOF;
		return tok;
	}

	tok->start = buffer;

	if(*buffer == '*'){
		//TODO(GI):
		//in future we may need to check here whether
		//we are dealing with a multiplication
		//or a dereferenced pointer
		//for now, * is always muliplication
		tok->type = DUC_TOK_MATH_OP;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '%'){
		tok->type = DUC_TOK_MATH_OP;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '/'){
		if( *(buffer + 1) == '/' ){
			tok->type = DUC_TOK_LINE_COMMENT;
			duc_get_next_char(d, buffer);
			duc_get_next_char(d, buffer);
			tok->len = 2;
			while(*buffer && *buffer != '\n' ){
				duc_get_next_char(d, buffer);
				tok->len += 1;
			}
			if(*buffer){
				d->line_nr += 1;
				tok->start_next = buffer + 1;
			}else{
				tok->start_next = buffer;
			}
			return tok;
		}else{
			tok->type = DUC_TOK_MATH_OP;
			tok->len = 1;
			tok->start_next = buffer + 1;
			return tok;
		}
	}

	if(*buffer == '+'){
		//NOTE(GI):
		//currently, we don't support unary '+'
		tok->type = DUC_TOK_MATH_OP;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '-'){
		tok->type = DUC_TOK_MATH_OP;
		char char_next = *(buffer + 1);
		if(char_next >= '0' && char_next <= '9'){
			duc_get_next_char(d, buffer);
			len += 1;
			//NOTE(GI):
			//rest of number will be caught by next if
		}else{
			tok->start_next = buffer + 1;
			return tok;
		}
	}

	if( *buffer >= '0' && *buffer <= '9' ){
		// tokenizer finds a number
		duc_get_next_char(d, buffer);
		len += 1;
		while( *buffer && *buffer >= '0' && *buffer <= '9' ){
			buffer += 1;
			len += 1;
		}

		tok->len = len;
		tok->type = DUC_TOK_NUMBER;
		tok->start_next = buffer;
		return tok;
	}

	if(*buffer == '='){
		if(*(buffer + 1) == '='){
			tok->type = DUC_TOK_REL_OP;
			tok->len = 2;
			tok->start_next = buffer + 2;
		}else{
			tok->type = DUC_TOK_EQUAL;
			tok->len = 1;
			tok->start_next = buffer + 1;
		}
		return tok;
	}

	if(*buffer == ';'){
		tok->type = DUC_TOK_DELIMETER;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == ','){
		tok->type = DUC_TOK_COMMA;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '('){
		tok->type = DUC_TOK_OPEN_BRACKET;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == ')'){
		tok->type = DUC_TOK_CLOSED_BRACKET;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '}'){
		tok->type = DUC_TOK_CLOSING_BLOCK;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '{'){
		tok->type = DUC_TOK_OPENING_BLOCK;
		tok->len = 1;
		tok->start_next = buffer + 1;
		return tok;
	}

	if(*buffer == '\"'){
		// tokenizer finds a string
		//NOTE(GI):
		//It is important that at this point we have already check for comments,
		//in strings are contained in comments
		char prev = *buffer;
		duc_get_next_char(d, buffer);
		len += 1;
		while(*buffer && *buffer != '\"'){
			prev = *buffer;
			duc_get_next_char(d, buffer);
			len += 1;
			if(*buffer && *buffer == '\"' && prev == '\\'){
				duc_get_next_char(d, buffer);
				len += 1;
			}
		}

		//sanity checks for the strings
		if(!(*buffer)){
			duc_user_error("line %d: Unexpected end of file", tok->line_nr);
			exit(0);
		}
		if(len > DUC_MAX_STRING_SIZE){
			duc_user_error("line %d: String is too long!", tok->line_nr);
			exit(0);
		}

		*buffer = 0;
		tok->start += 1;
		duc_string_unescape_double_quotes_and_others(tok->start);
		duc_get_next_char(d, buffer);
		tok->len = tec_string_length(tok->start);	//cannot just use variable len because of escaping
		tok->type = DUC_TOK_STRING;
		tok->start_next = buffer;
		return tok;
	}

	if(*buffer == '$'){
		// tokenizer finds a variable name
		duc_get_next_char(d, buffer);
		len += 1;
		while(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') || (*buffer >= '0' && *buffer <= '9') || (*buffer == '_') ) ){
			//TODO(GI):
			//our var can contain numerals 0 -9, just not begin with one ??
			len += 1;
			duc_get_next_char(d, buffer);
		}
		tok->len = len;
		tok->type = DUC_TOK_VAR_NAME;
		tok->start_next = buffer;
		return tok;
	}

	if(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') ) ){
		// tokenizer finds either keyword or function name

		while(*buffer && ( (*buffer >= 'a' && *buffer <= 'z') || (*buffer >= 'A' && *buffer <= 'Z') || (*buffer >= '0' && *buffer <= '9') || (*buffer == '_') ) ){
			//NOTE(GI):
			//our function name can contain numerals 0 -9 and underscore, just not begin with one
			len += 1;
			duc_get_next_char(d, buffer);
		}

		tok->len = len;

		while( tec_char_is_white_space(*buffer) ){
			duc_get_next_char(d, buffer);
		}

		if( *buffer == '(' ){
			if(duc_check_keyword(tok)){
				tok->start_next = buffer;
				return tok;
			}else{
				tok->type = DUC_TOK_FUNC_NAME;
				buffer += 1;
				tok->start_next = buffer;
				while( tec_char_is_white_space(*buffer)){
					duc_get_next_char(d, buffer);
				}
				if( *buffer == ')'){
					tok->empty_func = 1;
				}
			}

			return tok;
		}

		if( duc_check_datatype(tok) ){
			//tok->type = DUC_TOK_DATATYPE;	//done inside duc_check_datatype
			tok->start_next = buffer;
			return tok;
		}

		if(duc_check_keyword(tok)){
			tok->start_next = buffer;
			return tok;
		}
	}

	tok->type = DUC_TOK_ERROR;
	tok->start_next = buffer;
	return tok;

}//duc_parse_get_next_token*/

token* duc_parse_renege_token(token *tok){

	tok->start_next = tok->start;
	tok->start = tok->start_previous;
	tok->len = 0;
	tok->type = DUC_TOK_RENEGED;

	return tok;

}//duc_parse_renege_token*/

void duc_parse_function(ducx *d, token *tok){

	if( tok->len == 5 && tec_buf_begins(tok->start, "while") ){
		duc_parse_while(d, tok);
		return;
	}

	if( tok->len == 2 && tec_buf_begins(tok->start, "if") ){
		duc_parse_if(d, tok);
		return;
	}

	if( tok->len == 12 && tec_buf_begins(tok->start, "print_string") ){
		duc_parse_print_string(d, tok);
		return;
	}

	if( tok->len == 11 && tec_buf_begins(tok->start, "print_error") ){
		duc_parse_print_error(d, tok);
		return;
	}

	if( tok->len == 9 && tec_buf_begins(tok->start, "print_int") ){
		duc_parse_print_int(d, tok);
		return;
	}

	if( tok->len == 12 && tec_buf_begins(tok->start, "clear_screen") ){
		duc_parse_clear_screen(d, tok);
		return;
	}

	if( tok->len == 4 && tec_buf_begins(tok->start, "exit") ){
		duc_parse_exit(d, tok);
		return;
	}

	if( duc_parse_custom_function(d, tok, 0) ){
		return;
	}

	tok->start[tok->len] = 0;
	duc_error("%d: Unknown function %s\n", tok->line_nr, tok->start);
	exit(1);

}//duc_parse_function*/

void duc_parse_return(ducx *d, token *tok){

	if(!d->csp_counter){
		//redundant
		//should never run
		duc_error("line %d: return statement not in function\n", tok->line_nr);
		exit(1);
	}

	loc* l_to = d->csp[d->csp_counter - 1];
	loc *l = duc_parse_expression(d, tok, 0, 0);

	if(l->type == DUC_OP_INT_ASSIGN_C){
		l->ret.value.i = l->operand2.i64;
		l->type = DUC_OP_RETURN;
		l->operator.jump_to = l_to - d->block;	//d->block is first_loc;
		return;
	}
	if(l->type == DUC_OP_INT_ASSIGN_V){
		l->flags = DUC_FLAGS_RETURN_VAR;
		l->ret.value.i = l->operand2.i64;
		l->type = DUC_OP_RETURN;
		l->operator.jump_to = l_to - d->block;	//d->block is first_loc;
		return;
	}
	if(l->type == DUC_OP_INT_ASSIGN_L || (l->type > DUC_OP_BEGIN_OP && l->type < DUC_OP_END_OP) ){
		loc *l_prev = l;
		l = duc_parse_get_next_loc(d);
		l->operator.jump_to = l_to - d->block;	//d->block is first_loc;
		l->flags = DUC_FLAGS_RETURN_LOC;
		l->ret.value.l = l_prev;
		l->type = DUC_OP_RETURN;
		return;
	}

	duc_error("unforeseen return loc %d\n", l->type);
	exit(1);

}//duc_parse_return*/

void duc_parse_prototype(ducx *d, token *tok){

	duc_parse_get_next_token(d, tok);

	if(tok->type != DUC_TOK_FUNC_NAME){
		tok->start[tok->len] = 0;
		duc_user_error("line %d: %s does not appear to be a function name", tok->line_nr, tok->start);
		exit(0);
	}

	char *f_name = tok->start;
	f_name[tok->len] = 0;

	//NOTE(GI):
	//we are already past open bracket
	duc_parse_get_next_token(d, tok);

	//parsing parameters to be passed on
	int num_params = 0;
	int expect_more = 0;
	while(tok->type != DUC_TOK_CLOSED_BRACKET){
		expect_more = 0;
		if(tok->type != DUC_TOK_DATATYPE){
			duc_user_error("line %d: datatype expected while parsing function", tok->line_nr);
			exit(0);
		}
		duc_parse_get_next_token(d, tok);
		if(tok->type != DUC_TOK_VAR_NAME){
			tok->start[tok->len] = 0;
			duc_user_error("line %d: %s is not valid variable name", tok->line_nr, tok->start);
			exit(0);
		}
		//variable name checks happen inside duc_parse_var_add_list
		duc_parse_var_add_list(d, tok);
		duc_parse_get_next_token(d, tok);
		if(tok->type == DUC_TOK_COMMA){
			duc_parse_get_next_token(d, tok);
			expect_more = 1;
		}
		num_params += 1;
	}

	if(expect_more){
		duc_user_error("line %d: stray comma inside prototype", tok->line_nr);
		exit(0);
	}

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_OPENING_BLOCK){
		duc_user_error("line %d: function prototype to be followed by {", tok->line_nr);
		exit(0);
	}

	func *f = &(d->functions[d->last_function]);
	d->last_function += 1;

	f->name = f_name;
	f->num_params = num_params;
	f->name_len = tec_string_length(f_name);

//	if(tok->type == DUC_TOK_DELIMETER){
	//TODO(GI):
	//support declaring prototype of functions at top of file
//	}

	loc *l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_FUNC_BODY;
	d->in_function = 1;
	d->csp[d->csp_counter] = l;
	d->csp_counter += 1;
	f->l = l;

	if( tec_string_equal("main", f_name ) ){
		loc *first_l = &(d->block[0]);
		first_l->type = DUC_OP_JUMP;
		first_l->operator.jump_to = l - first_l;
	}

}//duc_parse_prototype*/

void duc_parse_var_clear_list(ducx *d){

	memset(d->v_list, 0, sizeof(var) * DUC_MAX_NUMBER_VARS );
	memset(d->v_list_names, 0, sizeof(char) * DUC_MAX_NUMBER_VARS * 32);
	memset(d->v_list_names_length, 0, sizeof(int) * DUC_MAX_NUMBER_VARS);
	d->num_vars = 0;

}//duc_parse_var_clear_list*/

var* duc_parse_var_add_list(ducx *d, token *tok){

	if(tok->type != DUC_TOK_VAR_NAME){
		duc_user_error("line %d: Invalid declaration of variable", tok->line_nr);
		exit(1);
	}

	if(tok->len > 31){
		duc_user_error("line %d: Max 31 characters for variable names\n", tok->line_nr);
		exit(0);
	}

	int hash = (tok->start[1] + tok->start[tok->len-1])%DUC_MAX_NUMBER_VARS;
	int loop = 0;	//this variable prevents infinite loop

	while( *(d->v_list_names[hash]) && loop < DUC_MAX_NUMBER_VARS ){
		if(d->v_list_names_length[hash] == tok->len && tec_buf_begins(tok->start, d->v_list_names[hash]) ){
			tok->start[tok->len] = 0;
			duc_user_error("line %d: Variable name %s already used\n", tok->line_nr, tok->start);
			exit(0);
		}
		hash += 1;
		if(hash == DUC_MAX_NUMBER_VARS){
			hash = 0;
		}
		loop += 1;
	}

	if(loop >= DUC_MAX_NUMBER_VARS){
		duc_user_error("Too many variables\n");
		exit(0);
	}

	memcpy(d->v_list_names[hash], tok->start, tok->len);
	d->v_list_names_length[hash] = tok->len;
	var *v = &(d->v_list[hash]);
	d->num_vars += 1;
	v->seq = d->num_vars;
//NOTE(GI):
//right now, all variables are int anyway
//	if( tec_buf_begins(str_type, "int") ){
//		v->type = DUC_VAR_INT;
//	}

	return v;

}//duc_parse_var_add_list*/

var* duc_parse_get_var(ducx *d, token *tok){

	if(tok->type != DUC_TOK_VAR_NAME){
		duc_error("Internal error, variable name expected");
		exit(1);
	}

	int hash = (tok->start[1] + tok->start[tok->len-1])%DUC_MAX_NUMBER_VARS;

	int loop = 0;	//this variable prevents infinite loop
	while( !( d->v_list_names_length[hash] == tok->len && tec_buf_begins(tok->start, d->v_list_names[hash])  ) && loop < DUC_MAX_NUMBER_VARS ){
		hash += 1;
		if(hash == DUC_MAX_NUMBER_VARS){
			hash = 0;
		}
		loop += 1;
	}

	if(loop >= DUC_MAX_NUMBER_VARS){
		tok->start[tok->len] = 0;
		duc_user_error("line %d: Variable %s not found\n", tok->line_nr, tok->start);
		exit(0);
		return NULL;
	}

	if( *(d->v_list_names[hash]) && d->v_list_names_length[hash] == tok->len && tec_buf_begins(tok->start, d->v_list_names[hash]) ){
		return &(d->v_list[hash]);
	}else{
		tok->start[tok->len] = 0;
		duc_user_error("line %d: Variable %s not found\n", tok->line_nr, tok->start);
		exit(0);
		return NULL;
	}

}//duc_parse_get_var*/

loc* duc_parse_custom_function(ducx *d, token *tok, int level){

	duc_parse_renege_token(tok);
	return duc_parse_expression(d, tok, level, 1);

}//duc_parse_custom_function*/

func* duc_parse_find_custom_function(ducx *d, token *tok){

	func *f = &(d->functions[0]);

	int not_found = 1;
	while(not_found && f->name){
		if( tok->len == f->name_len && tec_buf_begins(tok->start, f->name)){
			return f;
		}
		f += 1;
	}

	return 0;

}//duc_parse_find_custom_function*/

void duc_parse_create_variable(ducx *d, token *tok){

	duc_parse_get_next_token(d, tok);

	var *v = duc_parse_var_add_list(d, tok);

	//check for initialization
	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_EQUAL){
		duc_user_error("line %d: Please initialize your variables\n", tok->line_nr);
		exit(0);
	}

	loc *l = duc_parse_expression(d, tok, 0, 0);
	if(l->type != DUC_OP_FUNC_CALL_3){
		l->operand1.i64 = v->seq;
	}

	if(l->type >= DUC_OP_INT_ASSIGN_L && l->type <= DUC_OP_INT_ASSIGN_C){
		l->type -= DUC_OP_INT_ASSIGN_L;
		l->type += DUC_OP_CREATE_VAR_L;
		return;
	}

	if(l->type < DUC_OP_BEGIN_OP && l->type > DUC_OP_END_OP){
		duc_error("unexpected type");
		exit(1);
	}

	loc *prev_l = l;
	l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_CREATE_VAR_L;
	l->operand1.i64 = v->seq;
	l->operand2.l = prev_l;

}//duc_parse_create_variable*/

void duc_parse_statement(ducx *d, token *tok){

	var *v = duc_parse_get_var(d, tok);

	duc_parse_get_next_token(d, tok);

	if(tok->type != DUC_TOK_EQUAL){
		duc_user_error("line %d: Unrecognized statement", tok->line_nr);
		exit(0);
	}

	loc* l = duc_parse_expression(d, tok, 0, 0);
	if(!l){
		duc_error("loc is NULL");
		exit(1);
	}

	if(l->type == DUC_OP_FUNC_CALL_3){
		//NOTE(GI):
		//we treat DUC_OP_FUNC_CALL_3 differently here
		//because l->operand1 is in use in this loc
		//unlike all others between DUC_OP_BEGIN_OP and DUC_OP_END_OP
		loc *lo = l;
		l = duc_parse_get_next_loc(d);
		l->type = DUC_OP_INT_ASSIGN_L;
		l->operand1.i64 = v->seq;
		l->operand2.l = lo;
		return;
	}
	l->operand1.i64 = v->seq;

	if(l->type >= DUC_OP_INT_ASSIGN_L && l->type <= DUC_OP_INT_ASSIGN_C){
		return;
	}

	if(l->type < DUC_OP_BEGIN_OP && l->type > DUC_OP_END_OP){
		duc_error("unexpected type");
		exit(1);
	}

	return;

}//duc_parse_statement*/

loc* duc_parse_expression(ducx *d, token *tok, int level, int restrict_to_one){

	if(level > 63){
		duc_user_error("line %d: too many nested brackets\n", tok->line_nr);
		exit(0);
	}

	//NOTE(GI):
	//maximum 63 terms in an expression plus NULL at end
	term t[64];
	memset(t, 0, sizeof(t));
	int ti = 0;	//term index
	int ti_next = 0;
	loc *l = NULL;
	func *f = 0;
	int num_params = 0;

	while(tok->type != DUC_TOK_COMMA && tok->type != DUC_TOK_DELIMETER && tok->type != DUC_TOK_CLOSED_BRACKET){
		duc_parse_get_next_token(d, tok);
		switch(tok->type){
		case DUC_TOK_OPEN_BRACKET:
			l = duc_parse_expression(d, tok, level + 1, 0);
			t[ti].ret = l;
			duc_parse_get_next_token(d, tok);
			switch(tok->type){
			case DUC_TOK_MATH_OP:
				t[ti].op = tok->start[0];
				break;
			case DUC_TOK_DELIMETER:
			case DUC_TOK_CLOSED_BRACKET:
			case DUC_TOK_COMMA:
				//do nothing
				break;
			case DUC_TOK_REL_OP:
				t[ti].op = 'e';
				break;
			default:
				tok->start[tok->len] = 0;
				duc_user_error("line %d: unexpected token %s in expression (1)\n", tok->line_nr, tok->start);
				exit(1);
				break;
			}
			ti += 1;
			break;
		case DUC_TOK_VAR_NAME:
			t[ti].v[0] = duc_parse_get_var(d, tok);
			duc_parse_get_next_token(d, tok);
			switch(tok->type){
			case DUC_TOK_MATH_OP:
				t[ti].op = tok->start[0];
				break;
			case DUC_TOK_DELIMETER:
			case DUC_TOK_CLOSED_BRACKET:
			case DUC_TOK_COMMA:
				//do nothing
				break;
			case DUC_TOK_REL_OP:
				t[ti].op = 'e';
				break;
			default:
				tok->start[tok->len] = 0;
				duc_user_error("line %d: unexpected token %s in expression (2)\n", tok->line_nr, tok->start);
				exit(1);
				break;
			}
			ti += 1;
			break;
		case DUC_TOK_NUMBER:
			t[ti].value[0].i = tec_string_to_int(tok->start);
			duc_parse_get_next_token(d, tok);
			switch(tok->type){
			case DUC_TOK_MATH_OP:
				t[ti].op = tok->start[0];
				break;
			case DUC_TOK_DELIMETER:
			case DUC_TOK_CLOSED_BRACKET:
			case DUC_TOK_COMMA:
				//do nothing
				break;
			case DUC_TOK_REL_OP:
				t[ti].op = 'e';
				break;
			default:
				tok->start[tok->len] = 0;
				duc_user_error("line %d: unexpected token %s in expression (3)\n", tok->line_nr, tok->start);
				exit(1);
				break;
			}
			ti += 1;
			break;
		case DUC_TOK_FUNC_NAME:
			f = duc_parse_find_custom_function(d, tok);
			t[ti].f = f;
			if(!f){
				tok->start[tok->len] = 0;
				duc_user_error("line %d: function %s not found (no built-in functions currently allowed in expressions)", tok->line_nr, tok->start);
				exit(0);
			}
			if(tok->empty_func){
				duc_parse_get_next_token(d, tok);	//should give us closing bracket
			}else{
				num_params = 0;
				t[ti].operands[num_params] = duc_parse_expression(d, tok, level + 1, 0);
				num_params = 1;
				while(num_params < f->num_params && tok->type == DUC_TOK_COMMA){
					tok->type = 0;	//!!!!, get rid of that comma to start duc_parse_expression
					t[ti].operands[num_params] = duc_parse_expression(d, tok, level + 1, 0);
					num_params += 1;
				}
			}
			duc_parse_get_next_token(d, tok);
			switch(tok->type){
			case DUC_TOK_MATH_OP:
				t[ti].op = tok->start[0];
				break;
			case DUC_TOK_DELIMETER:
			case DUC_TOK_CLOSED_BRACKET:
			case DUC_TOK_COMMA:
				//do nothing
				break;
			case DUC_TOK_REL_OP:
				t[ti].op = 'e';
				break;
			default:
				tok->start[tok->len] = 0;
				duc_user_error("line %d: unexpected token %s in expression (4)\n", tok->line_nr, tok->start);
				exit(1);
				break;
			}
			ti += 1;
			break;
		default:
			tok->start[tok->len] = 0;
			duc_user_error("line %d: unexpected token %s in expression (5):\n", tok->line_nr, tok->start);
			exit(1);
			break;
		}
	}

	if(!ti){
		duc_user_error("line %d: Empty or badly formed expression\n", d->line_nr);
		exit(0);
	}

	if(restrict_to_one && ti != 1){
		duc_user_error("line %d: Malformed expression, nothing should follow that function call\n", d->line_nr);
		exit(0);
	}

	if(ti == 1){

		if(t[0].f){

			uint64_t final_operands[3];
			duc_parse_process_parameters(d, t[0].f->num_params, t[0].operands, final_operands);

			l = duc_parse_get_next_loc(d);
			l->type = DUC_OP_FUNC_CALL_3;
			l->operand1.i64 = final_operands[0];
			l->operand2.i64 = final_operands[1];
			l->operand3.i64 = final_operands[2];
			l->operator.jump_to = t[0].f->l - d->block;
			return l;
		}

		l = duc_parse_get_next_loc(d);
		l->type = DUC_OP_INT_ASSIGN_V;
		if(t[0].ret){
			l->operand2.l = t[0].ret;
			l->type -= 1;	//assign l
		}else{
			if(t[0].v[0]){
				l->operand2.i64 = t[0].v[0]->seq;
			}else{
				l->operand2.i64 = t[0].value[0].i;
				l->type += 1;	//assign c
			}
		}
		return l;
	}

	ti = 0;
	int not_last = 1;
	while(not_last){
		if(!t[ti].op){
			//when dealing with functions,
			//we also need to include the last term in the loop
			//and the last term will have t[ti].op == NULL
			not_last = 0;
		}
		if(t[ti].f){	//check for functions

			uint64_t final_operands[3];
			duc_parse_process_parameters(d, t[ti].f->num_params, t[ti].operands, final_operands);

			l = duc_parse_get_next_loc(d);
			l->type = DUC_OP_FUNC_CALL_3;
			l->operand1.i64 = final_operands[0];
			l->operand2.i64 = final_operands[1];
			l->operand3.i64 = final_operands[2];
			l->operator.jump_to = t[ti].f->l - d->block;
			t[ti].ret = l;
//			t[ti].op = ' ';
		}
		ti += 1;
	}

	ti = 0;
	while(t[ti].op){//null when at end of terms
		if(t[ti].op == '*' || t[ti].op == '/' || t[ti].op == '%'){
			ti_next = ti + 1;
			while(t[ti_next].op == ' '){
				ti_next += 1;
			}
			//produce bytecode here
			l = duc_parse_get_next_loc(d);
			if(t[ti].op == '*'){
				l->type = DUC_OP_INT_MUL_VV;
			}else{
				if(t[ti].op == '%'){
					l->type = DUC_OP_INT_MOD_VV;
				}else{
					l->type = DUC_OP_INT_DIV_VV;
				}
			}
			//TODO(GI):
			//consider *_CC case where we execute 
			//operation right here
			//TODO(GI):
			//remove locs with identity elements
			//i.e. + 0 and * 1
			if(t[ti].ret){
				l->operand2.l = t[ti].ret;
				l->type -= 3;
			}else{
				if(t[ti].v[0]){
					l->operand2.i64 = t[ti].v[0]->seq;
				}else{
					l->operand2.i64 = t[ti].value[0].i;
					l->type += 3;	//sets it from *_VV to *_CV
				}
			}
			if(t[ti_next].ret){
				l->operand3.l = t[ti_next].ret;
				l->type -= 1;	// sets it from *_LV to *_LL
			}else{
				if(t[ti_next].v[0]){
					l->operand3.i64 = t[ti_next].v[0]->seq;
				}else{
					l->operand3.i64 = t[ti_next].value[0].i;
					l->type += 1;	//sets it from *_VV to *_VC
				}
			}
			t[ti_next].ret = l;
			t[ti].op = ' ';
		}
		ti += 1;
	}

	ti = 0;
	while(t[ti].op){

		if(t[ti].op == '+' || t[ti].op == '-'){
			ti_next = ti + 1;
			while(t[ti_next].op == ' '){
				ti_next += 1;
			}
			//produce bytecode here
			l = duc_parse_get_next_loc(d);
			l->type = DUC_OP_INT_ADD_VV;
			if(t[ti].op == '-'){
				l->type = DUC_OP_INT_SUB_VV;
			}
			if(t[ti].ret){
				l->operand2.l = t[ti].ret;
				l->type -= 3;
			}else{
				if(t[ti].v[0]){
					l->operand2.i64 = t[ti].v[0]->seq;
				}else{
					l->operand2.i64 = t[ti].value[0].i;
					l->type += 3;	//sets it from *_VV to *_CV
				}
			}
			if(t[ti_next].ret){
				l->operand3.l = t[ti_next].ret;
				l->type -= 1;	// sets it from *_LV to *_LL
			}else{
				if(t[ti_next].v[0]){
					l->operand3.i64 = t[ti_next].v[0]->seq;
				}else{
					l->operand3.i64 = t[ti_next].value[0].i;
					l->type += 1;	//sets it from *_VV to *_VC
				}
			}
			t[ti_next].ret = l;
			t[ti].op = ' ';
		}
		ti += 1;
	}

	//NOTE(GI):
	//checkig for relative operators
	//so far, only == supported, encoded as 'e'
	ti = 0;
	while(t[ti].op){

		if(t[ti].op == 'e'){
			ti_next = ti + 1;
			while(t[ti_next].op == ' '){
				ti_next += 1;
			}
			//produce bytecode here
			l = duc_parse_get_next_loc(d);
			l->type = DUC_OP_REL_EQ_VV;
			if(t[ti].ret){
				l->operand2.l = t[ti].ret;
				l->type -= 3;
			}else{
				if(t[ti].v[0]){
					l->operand2.i64 = t[ti].v[0]->seq;
				}else{
					l->operand2.i64 = t[ti].value[0].i;
					l->type += 3;	//sets it from *_VV to *_CV
				}
			}
			if(t[ti_next].ret){
				l->operand3.l = t[ti_next].ret;
				l->type -= 1;	// sets it from *_LV to *_LL
			}else{
				if(t[ti_next].v[0]){
					l->operand3.i64 = t[ti_next].v[0]->seq;
				}else{
					l->operand3.i64 = t[ti_next].value[0].i;
					l->type += 1;	//sets it from *_VV to *_VC
				}
			}
			t[ti_next].ret = l;
			t[ti].op = ' ';//redundant as no more ops left, doesn't hurt
		}
		ti += 1;
	}

	return l;

}//duc_parse_expression*/

void duc_parse_process_parameters(ducx *d, int num_params, loc **parameters, uint64_t *final_operands){

	int i = 0;
	int linloc = 0;
	loc *l = NULL;
	while(i < num_params){
		final_operands[i] = (parameters[i])->operand2.i64;

		if(parameters[i]->type != DUC_OP_INT_ASSIGN_C){
			if(!linloc){
				l = duc_parse_get_next_loc(d);
				l->type = DUC_OP_LINLOC;
				linloc = 1;
			}
			if(parameters[i]->type == DUC_OP_INT_ASSIGN_V){
				switch(i){
				case 0:
					l->operand1.i64 = (parameters[i])->operand2.i64;
					break;
				case 1:
					l->operand2.i64 = (parameters[i])->operand2.i64;
					break;
				case 2:
					l->operand3.i64 = (parameters[i])->operand2.i64;
					break;
				}
			}else{
				while(parameters[i]->type == DUC_OP_INT_ASSIGN_L){
					parameters[i] = parameters[i]->operand2.l;
				}
				if(parameters[i]->type > DUC_OP_BEGIN_OP && parameters[i]->type < DUC_OP_END_OP){
					final_operands[i] = (uint64_t) parameters[i];
					switch(i){
					case 0:
						l->operand1.l = parameters[i];
						l->flags |= DUC_FLAGS_FIRST_LOC;
						break;
					case 1:
						l->operand2.l = parameters[i];
						l->flags |= DUC_FLAGS_SECOND_LOC;
						break;
					case 2:
						l->operand3.l = parameters[i];
						l->flags |= DUC_FLAGS_THIRD_LOC;
						break;
					}
				}else{
					duc_error("line %d: unexpected loc->type %d at end of expression\n", d->line_nr, (parameters[i])->type);
					exit(1);
				}
			}
		}
		i += 1;
	}

}//duc_parse_process_parameters*/

void duc_parse_while(ducx *d, token *tok){

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_OPEN_BRACKET){
		duc_user_error("line %d: while must be followed by an open bracket\n", tok->line_nr);
		exit(0);
	}

	loc *condition_start = duc_parse_peek_next_loc(d);
	loc *condition = duc_parse_expression(d, tok, 0, 0);
	loc *l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_WHILE;
	l->operand1.l = condition;

	d->csp[d->csp_counter] = l;
	d->conditions[d->csp_counter] = condition_start;
	d->csp_counter += 1;

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_OPENING_BLOCK){
		duc_user_error("line %d: after while must come open curly brace {", tok->line_nr);
		exit(0);
	}

}//duc_parse_while*/

void duc_parse_if(ducx *d, token *tok){

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_OPEN_BRACKET){
		duc_user_error("line %d: if must be followed by an open bracket\n", tok->line_nr);
		exit(0);
	}

	loc *condition_start = duc_parse_peek_next_loc(d);
	loc *condition = duc_parse_expression(d, tok, 0, 0);
	loc *l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_IF;
	l->operand1.l = condition;

	d->csp[d->csp_counter] = l;
	d->conditions[d->csp_counter] = condition_start;
	d->csp_counter += 1;

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_OPENING_BLOCK){
		duc_user_error("line %d: after if must come open curly brace {\n", tok->line_nr);
		exit(0);
	}

}//duc_parse_if*/

void duc_parse_else(ducx *d, token *tok, loc *last_if){

	if(!d->csp_counter){
		duc_user_error("line %d: Encountered else, incorrect nesting of control structures (1)\n", tok->line_nr);
		exit(0);
	}

//	last_if = d->csp[d->csp_counter - 1];
	if(!last_if || last_if->type != DUC_OP_IF){
		duc_debug_print_block(d, last_if, 1);
		duc_user_error("line %d: Encountered else, incorrect nesting of control structures (2)\n", tok->line_nr);
		exit(0);
	}

	last_if->type = DUC_OP_IF_ELSE;
	loc *l_else = duc_parse_peek_next_loc(d);

	loc *first_loc = &(d->block[0]);
	last_if->operand3.i64 = l_else - first_loc;

	d->csp[d->csp_counter] = last_if;
	d->csp_counter += 1;

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_OPENING_BLOCK){
		duc_user_error("after else must come open curly brace {");
		exit(1);
	}

}//duc_parse_else*/

void duc_parse_print_string(ducx *d, token *tok){

	//TODO(GI):
	//good enough for now
	//these built-in functions should get an overhaul in later versions

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_STRING && tok->type != DUC_TOK_VAR_NAME){
		duc_user_error("print_string() should get a string");
		exit(0);
	}

	loc *l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_FUNC_3;
	l->operator.op_func = 2;//&duc_exec_print_string;

	if(tok->type == DUC_TOK_VAR_NAME ){
		duc_user_error("cannot currently pass on var to print_string");
		exit(0);
	}

	if(tok->type == DUC_TOK_STRING ){
		l->operand2.i64 = (int64_t) tok->start;
		l->operand3.i64 = tok->len;
	}

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_CLOSED_BRACKET){
		duc_user_error("print_string function should end with closing bracket\n");
		exit(0);
	}
	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_DELIMETER){
		duc_user_error("statements should end with delimeters\n");
		exit(0);
	}

}//duc_parse_print_string*/

void duc_parse_print_int(ducx *d, token *tok){

	//TODO(GI):
	//good enough for now
	//these built-in functions should get an overhaul in later versions

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_NUMBER && tok->type != DUC_TOK_VAR_NAME){
		duc_user_error("print_int() should get an int");
		exit(1);
	}

	loc *l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_FUNC_3;

	if(tok->type == DUC_TOK_VAR_NAME ){
		//TODO(GI):
		// check we have an int and not something else
		var *v = duc_parse_get_var(d, tok);
		l->operand1.i64 = v->seq;
		l->type = DUC_OP_LINLOC;
		l = duc_parse_get_next_loc(d);
		l->type = DUC_OP_FUNC_3;
		l->operator.op_func = 3;//&duc_exec_print_int;
	}

	if(tok->type == DUC_TOK_NUMBER ){
		l->operand1.i64 = tec_string_to_int(tok->start);
		l->operator.op_func = 3;//&duc_exec_print_int;
	}

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_CLOSED_BRACKET){
		duc_user_error("print_int() should end with closing bracket\n");
		exit(0);
	}
	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_DELIMETER){
		duc_user_error("print_int() should be followed by delimeter\n");
		exit(0);
	}

	return;

}//duc_parse_print_int*/

void duc_parse_clear_screen(ducx *d, token *tok){

	//TODO(GI):
	//good enough for now
	//these built-in functions should get an overhaul in later versions

	if(!tok->empty_func){
		duc_user_error("clear_screen() takes no arguments");
		exit(0);
	}

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_CLOSED_BRACKET){
		duc_user_error("clear_screen() should have closing bracket\n");
		exit(0);
	}

	loc *l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_FUNC_0;
	l->operator.op_func = 1;//&duc_exec_clear_screen;

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_DELIMETER){
		duc_user_error("clear_screen() should be followed by delimeter\n");
		exit(0);
	}

	return;

}//duc_parse_clear_screen*/

void duc_parse_exit(ducx *d, token *tok){

	//TODO(GI):
	//good enough for now
	//these built-in functions should get an overhaul in later versions

	duc_parse_get_next_token(d, tok);

	if(tok->type != DUC_TOK_NUMBER && tok->type != DUC_TOK_VAR_NAME){
		duc_user_error("exit() should get an int");
		exit(0);
	}

	if(tok->type == DUC_TOK_VAR_NAME){
		loc *l = duc_parse_get_next_loc(d);
		l->type = DUC_OP_LINLOC;
		var *v = duc_parse_get_var(d, tok);
		l->operand1.i64 = v->seq;
	}

	loc *l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_FUNC_3;
	l->operator.op_func = 5;

	if(tok->type == DUC_TOK_NUMBER ){
		l->operand1.i64 = tec_string_to_int(tok->start);
	}

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_CLOSED_BRACKET){
		duc_user_error("exit() should end with closing bracket\n");
		exit(0);
	}
	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_DELIMETER){
		duc_user_error("exit() should be followed by delimeter\n");
		exit(0);
	}

	return;

}//duc_parse_exit*/

void duc_parse_print_error(ducx *d, token *tok){

	//TODO(GI):
	//good enough for now
	//these built-in functions should get an overhaul in later versions

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_STRING && tok->type != DUC_TOK_VAR_NAME){
		duc_user_error("print_error() takes a string");
		exit(0);
	}

	loc *l = duc_parse_get_next_loc(d);
	l->type = DUC_OP_FUNC_3;
	l->operator.op_func = 4;

	if(tok->type == DUC_TOK_VAR_NAME ){
		duc_user_error("cannot currently pass on var to print_string");
		exit(0);
	}

	if(tok->type == DUC_TOK_STRING ){
		l->operand2.i64 = (int64_t) tok->start;
		l->operand3.i64 = tok->len;
	}else{
		duc_user_error("for now, only literal strings allowed in this function\n");
		exit(0);
	}

	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_CLOSED_BRACKET){
		duc_user_error("print_error() should end with closing bracket\n");
		exit(0);
	}
	duc_parse_get_next_token(d, tok);
	if(tok->type != DUC_TOK_DELIMETER){
		duc_user_error("print_error() should be followed by delimeter\n");
		exit(0);
	}

}//duc_parse_print_error*/

void duc_run_bytecode(ducx *d){

	void (*func_0)(var *ret);
	void (*func_v)(loc *l, var *ret);

	var return_value;

	loc *l = &(d->block[0]);
	loc *first_loc = l;
	loc *lo = NULL;
	loc *lc = NULL;
	int increment = 1;

	loc **bp = (loc **) d->stack;
	int64_t *st = (int64_t *) d->stack;
	int64_t *vars = (int64_t *) d->stack;
	int *tmp_op1 = NULL;
	int *tmp_op2 = NULL;
	int *tmp_op3 = NULL;
	st += 1;

	if(!l->type){
		tec_error("No main function detected");
		return;
	}

	while(l->type){

		switch(l->type){
		case DUC_OP_CREATE_VAR_L:
			lo = l->operand2.l;
			l->ret.value.i = lo->ret.value.i;
			(vars[l->operand1.i64]) = (int64_t) l->ret.value.i;
			st += 1;
			break;
		case DUC_OP_CREATE_VAR_V:
			if(l->operand2.i64){
				l->ret.value.i = (int32_t) (vars[l->operand2.i64]);
			}
			(vars[l->operand1.i64]) = (int64_t) l->ret.value.i;
			st += 1;
			break;
		case DUC_OP_CREATE_VAR_C:
			l->ret.value.i = (int32_t) l->operand2.i64;
			(vars[l->operand1.i64]) = (int64_t) l->ret.value.i;
			st += 1;
			break;
		case DUC_OP_INT_ASSIGN_L:
			lo = (loc *) l->operand2.l;
			l->ret.value.i = lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ASSIGN_V:
			l->ret.value.i = vars[l->operand2.i64];
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ASSIGN_C:
			l->ret.value.i = (int32_t) l->operand2.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_JUMP:
			l = first_loc + (int) l->operator.jump_to;
			if(l->type == DUC_OP_IF || l->type == DUC_OP_IF_ELSE){
				//here we use the i64 part of the union
				//compiler doesn't like addition between loc * and loc *
				l = l->operand2.i64 + first_loc;
			}
			increment = 0;
			break;
		case DUC_OP_WHILE:
			lo = l->operand1.l;
			if(lo->ret.value.i){
				l += 1;
			}else{
				l = l->operand2.i64 + first_loc;
			}
			increment = 0;
			break;
		case DUC_OP_IF:
			lo = l->operand1.l;
			if(lo->ret.value.i){
				l += 1;
			}else{
				l = l->operand2.i64 + first_loc;
			}
			increment = 0;
			break;
		case DUC_OP_IF_ELSE:
			lo = l->operand1.l;
			if(lo->ret.value.i){
				l += 1;
			}else{
				if(l->operand3.i64){
					l = l->operand3.i64 + first_loc;
				}else{
					l = l->operand2.i64 + first_loc;
				}
			}
			increment = 0;
			break;
		case DUC_OP_LINLOC:
			lo = l + 1;
			if(l->operand1.i64){
				if(l->flags&DUC_FLAGS_FIRST_LOC){
					lo->operand1.i64 = l->operand1.l->ret.value.i64;
				}else{
					lo->operand1.v = (var *) (vars[l->operand1.i64]);
				}
			}
			if(l->operand2.i64){
				if(l->flags&DUC_FLAGS_SECOND_LOC){
					lo->operand2.i64 = l->operand2.l->ret.value.i64;
				}else{
					lo->operand2.v = (var *) (vars[l->operand2.i64]);
				}
			}
			if(l->operand3.i64){
				if(l->flags&DUC_FLAGS_THIRD_LOC){
					lo->operand3.i64 = l->operand3.l->ret.value.i64;
				}else{
					lo->operand3.v = (var *) (vars[l->operand3.i64]);
				}
			}
			break;
		case DUC_OP_FUNC_CALL_3:
			*st = (int64_t) bp;	//place where we jump back to once function is returning
			st += 1;
			bp = (loc **) st;
			lo = l; //current instruction on to be placed on stack
			*bp = lo;
			vars = st;
			st += 1;

			if(l->operand1.i64){
				vars[1] = l->operand1.i64;
				st += 1;
			}
			if(l->operand2.i64){
				vars[2] = l->operand2.i64;
				st += 1;
			}
			if(l->operand3.i64){
				vars[3] = l->operand3.i64;
				st += 1;
			}

			l = l->operator.jump_to + first_loc;
			increment = 0;
			break;
		case DUC_OP_FUNC_BODY:
			break;
		case DUC_OP_RETURN:
			if(l->flags&DUC_FLAGS_RETURN_VAR){
				return_value.value.i = vars[l->ret.value.i];
			}else{
				if(l->flags&DUC_FLAGS_RETURN_LOC){
					loc *lc = l->ret.value.l;
					return_value.value.i64 = lc->ret.value.i64;
				}else{
					return_value.value.i = l->ret.value.i;
				}
			}

			if(bp == (loc **) d->stack){
				exit(return_value.value.i);
			}

			lo = *bp;
			lo->ret.value.i = return_value.value.i;

			while((loc **) st > bp){
				*st = 0;
				st -= 1;
			}
			l = lo + 1;
			st -= 1;
			bp -= 1;
			bp = (loc **) *bp;
			vars = (int64_t *) bp;
			increment = 0;
			break;
		case DUC_OP_FUNC_0:
			//these functions take zero arguments
			//not including space to store return value
			func_0 = built_in_functions[(int64_t) l->operator.op_func];
			func_0( &(l->ret) );
			break;
		case DUC_OP_FUNC_3:
			//NOTE(GI):
			//this will only work with built-in functions
			//for custom functions, there should be no operator
			func_v = built_in_functions[(int64_t) l->operator.op_func];
			func_v( l, &(l->ret) );
			break;
		case DUC_OP_FUNC_11:
			//not yet implemented
			break;
		case DUC_OP_INT_ADD_LL:
			lo = (loc *) l->operand2.l;
			lc = (loc *) l->operand3.l;
			l->ret.value.i = lo->ret.value.i + lc->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_LV:
			lo = l->operand2.l;
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = lo->ret.value.i + *tmp_op3;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_LC:
			lo = l->operand2.l;
			l->ret.value.i = lo->ret.value.i + (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_VL:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			lo = l->operand3.l;
			l->ret.value.i = *tmp_op2 + lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_VV:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = *tmp_op2 + *tmp_op3;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_VC:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			l->ret.value.i = *tmp_op2 + (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_CL:
			lo = l->operand3.l;
			l->ret.value.i = (int)l->operand2.i64 + lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_CV:
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = (int)l->operand2.i64 + *tmp_op3;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_ADD_CC:
			l->ret.value.i = (int)l->operand2.i64 + (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_LL:
			lo = (loc *) l->operand2.l;
			lc = (loc *) l->operand3.l;
			l->ret.value.i = lo->ret.value.i - lc->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_LV:
			lo = (loc *) l->operand2.l;
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = lo->ret.value.i - *tmp_op3;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_LC:
			lo = l->operand2.l;
			l->ret.value.i = lo->ret.value.i - (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_VL:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			lo = l->operand3.l;
			l->ret.value.i = *tmp_op2 - lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_VV:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = *tmp_op2 - *tmp_op3;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_VC:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			l->ret.value.i = *tmp_op2 - (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_CL:
			lo = l->operand3.l;
			l->ret.value.i = (int)l->operand2.i64 - lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_CV:
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = (int)l->operand2.i64 - *tmp_op3;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_SUB_CC:
			l->ret.value.i = (int)l->operand2.i64 - (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_LL:
			lo = l->operand2.l;
			lc = l->operand3.l;
			l->ret.value.i = lo->ret.value.i * lc->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_LV:
			lo = l->operand2.l;
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = lo->ret.value.i * (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_LC:
			lo = l->operand2.l;
			l->ret.value.i = lo->ret.value.i * (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_VL:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			lo = l->operand3.l;
			l->ret.value.i = *tmp_op2 * lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_VV:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = (*tmp_op2) * (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_VC:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			l->ret.value.i = *tmp_op2 * (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_CL:
			lo = l->operand3.l;
			l->ret.value.i = (int)l->operand2.i64 * lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_CV:
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = (int)l->operand2.i64 * (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MUL_CC:
			l->ret.value.i = (int)l->operand2.i64 * (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_LL:
			lo = l->operand2.l;
			lc = l->operand3.l;
			if(!lc || !lc->ret.value.i){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = lo->ret.value.i / lc->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_LV:
			lo = l->operand2.l;
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			if(l->operand3.i64 || !(*tmp_op3) ){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = lo->ret.value.i / (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_LC:
			lo = l->operand2.l;
			if(! l->operand3.i64){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = lo->ret.value.i / (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_VL:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			lo = l->operand3.l;
			if(!lo || !lo->ret.value.i){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (*tmp_op2) / lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_VV:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			if(!l->operand3.i64 || !(*tmp_op3) ){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (*tmp_op2) / (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_VC:
			if(! l->operand3.i64){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			l->ret.value.i = (*tmp_op2) / (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_CL:
			lo = l->operand3.l;
			if(!lo || !lo->ret.value.i){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (int)l->operand2.i64 / lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_CV:
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			if(!l->operand3.i64  || !(*tmp_op3) ){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (int)l->operand2.i64 / (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_DIV_CC:
			if(! l->operand3.i64){
				duc_user_error("Do not divide by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (int)l->operand2.i64 / (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_LL:
			lo = l->operand2.l;
			lc = l->operand3.l;
			if(!lc || lc->ret.value.i){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = lo->ret.value.i % lc->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_LV:
			lo = l->operand2.l;
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			if(!l->operand3.i64 || !(*tmp_op3) ){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = lo->ret.value.i % (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_LC:
			lo = l->operand2.l;
			if(!l->operand3.i64){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = lo->ret.value.i % (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_VL:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			lo = l->operand3.l;
			if(!lo || !lo->ret.value.i){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (*tmp_op2) % lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_VV:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			if(!l->operand3.i64 || !(*tmp_op3) ){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (*tmp_op2) % (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_VC:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			if(!l->operand3.i64){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (*tmp_op2) % (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_CL:
			lo = l->operand3.l;
			if(!lo || !(lo->ret.value.i) ){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (int)l->operand2.i64 % lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_CV:
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			if(!l->operand3.i64 || !(*tmp_op3) ){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (int)l->operand2.i64 % (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_INT_MOD_CC:
			if(!l->operand3.i64){
				duc_user_error("Do not divide or mod by %s. This will create a black hole and destroy the universe.\n", "zero");
				exit(0);
			}
			l->ret.value.i = (int)l->operand2.i64 % (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_LL:
			lo = l->operand2.l;
			lc = l->operand3.l;
			l->ret.value.i = lo->ret.value.i == lc->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_LV:
			lo = l->operand2.l;
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = lo->ret.value.i == (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_LC:
			lo = l->operand2.l;
			l->ret.value.i = lo->ret.value.i == (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_VL:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			lo = l->operand3.l;
			l->ret.value.i = (*tmp_op2) == lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_VV:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = (*tmp_op2) == (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_VC:
			tmp_op2 = (int *) &(vars[l->operand2.i64]);
			l->ret.value.i = (*tmp_op2) == (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_CL:
			lo = l->operand3.l;
			l->ret.value.i = (int)l->operand2.i64 == lo->ret.value.i;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_CV:
			tmp_op3 = (int *) &(vars[l->operand3.i64]);
			l->ret.value.i = (int)l->operand2.i64 == (*tmp_op3);
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		case DUC_OP_REL_EQ_CC:
			l->ret.value.i = (int)l->operand2.i64 == (int)l->operand3.i64;
			if(l->operand1.i64){
				vars[l->operand1.i64] = l->ret.value.i;
			}
			break;
		}

		//TODO(GI):
		//more complex logic will be needed here
		//once we use loc_long
		if(increment){
			l += 1;
		}else{
			increment = 1;
		}
	}

}//duc_run_bytecode*/

void duc_exec_print_string(loc *l, var *ret){

	char *str = (char *) l->operand2.i64;
	write(1, str, (size_t) l->operand3.i64);

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_string*/

void duc_exec_print_int(loc *l, var *ret){

	char tmp[13];
	int n = tec_string_from_int( (int) l->operand1.i64, tmp, 12);
//	tmp[n] = 10;
//	n += 1;
	tmp[n] = 0;
	write(1, tmp, n);

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_int*/

void duc_exec_clear_screen(var *ret){
#ifdef __linux
	tc_clear_screen();
#else
	write(1, "\033[H\033[2J\033[3J", 11);
#endif

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_clear_screen*/

void duc_exec_exit(loc *l, var *ret){

	exit((int) l->operand1.i64);

}//duc_exec_exit*/

void duc_exec_print_error(loc *l, var *ret){

	char tmp[7] = "x[1;31m";
	tmp[0] = 27;
	write(2, tmp, 7);

	char *str = (char *) l->operand2.i64;
	write(2, str, (size_t) l->operand3.i64);

	char tmp2[5] = "x[0m";
	tmp2[0] = 27;
	(void) write(2, tmp2, 5);

	ret->type = DUC_VAR_INT;
	ret->value.i = 1;

}//duc_exec_print_error*/

char* duc_string_unescape_double_quotes_and_others(char *str){

	//TODO(GI):
	//I am sure this function needs more work
	//good enough for now

	if(!str)
		return str;

	char hex[3];

	char *original = str;
	while(*str){
		if(*str == '\\' && str[1] == '\"'){
			tec_string_shift(str);
		}
		if(*str == '\\' && str[1] == 'n'){
			//new line
			tec_string_shift(str);
			*str = 10;
		}
		if(*str == '\\' && str[1] == 't'){
			//tab
			tec_string_shift(str);
			*str = 9;
		}
		if(*str == '\\' && str[1] == 'r'){
			//carriage return
			tec_string_shift(str);
			*str = 13;
		}
		if(*str == '\\' && str[1] == 'x'){
			//hex number
			tec_string_shift(str);
			tec_string_shift(str);
			int num_bytes;
			int num = tec_string_hex_to_int(str, &num_bytes);
			if(num_bytes != 2){
				duc_user_error("\\x not followed by 2 digits\n");
			}
			tec_string_shift(str);
			*str = num;
		}

		str += 1;
	}

	return original;

}//duc_string_unescape_double_quotes_and_others*/

int duc_check_datatype(token *tok){

	if(!tok || !tok->start[0])
		return 0;

	if( tok->len == 3 && tec_buf_begins(tok->start, "int") ){
		tok->type = DUC_TOK_DATATYPE;
		return 1;
	}
	if( tok->len == 4 && tec_buf_begins(tok->start, "char") ){
		tok->type = DUC_TOK_DATATYPE;
		duc_user_error("line %d: char not currently supported\n", tok->line_nr);
		exit(0);
	}
	if( tok->len == 5 && tec_buf_begins(tok->start, "float") ){
		tok->type = DUC_TOK_DATATYPE;
		duc_user_error("line %d: float not currently supported\n", tok->line_nr);
		exit(0);
	}

	return 0;

}//duc_check_datatype*/

int duc_check_keyword(token *tok){

	if(!tok || !tok->start[0])
		return 0;

	if( tok->len == 5 && tec_buf_begins(tok->start, "while") ){
		tok->type = DUC_TOK_OTHER_KEYWORD;
		return 1;
	}
	if( tok->len == 2 && tec_buf_begins(tok->start, "if") ){
		tok->type = DUC_TOK_OTHER_KEYWORD;
		return 1;
	}
	if( tok->len == 6 && tec_buf_begins(tok->start, "return") ){
		tok->type = DUC_TOK_RETURN;
		return 1;
	}
	if( tok->len == 4 && tec_buf_begins(tok->start, "else") ){
		tok->type = DUC_TOK_ELSE;
		return 1;
	}

	return 0;

}//duc_check_keyword*/
